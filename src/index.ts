import * as express from 'express';
import sum from './utils/sum';

const app = express();
const id = Math.random().toString(16).slice(2, 6);

app.listen(3000, () => {
  console.log('listening on 3000');
});

app.get('/', (_req, res) => {
  res.send(`Hello, I'm ${id}`);
});

app.get('/sum/:a/:b', (req, res) => {
  const { a, b } = req.params;
  try {
    res.send(`${a} + ${b} = ${sum(parseInt(a), parseInt(b))}`);
  } catch (e) {
    console.error(e);
  }
});
