import sum from './sum';

describe('Simple operations', () => {
  it('should calculate a sum correclty', (): void => {
    expect(sum(1, 2)).toBe(3);
  });
});
